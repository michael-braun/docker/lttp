package main

import (
	"net/http"
	"os"
)

func handleHeartbeat(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(200)
}

func main() {
	rootPath := os.Getenv("ROOT_PATH")

	if rootPath == "" {
		return
	}

	http.Handle("/", http.FileServer(http.Dir(rootPath)))
	if _, err := os.Stat(rootPath + "/heartbeat"); err != nil {
		http.HandleFunc("/heartbeat", handleHeartbeat)
	}
	http.ListenAndServe(":8080", nil)
}
