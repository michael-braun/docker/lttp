FROM alpine:3.8

MAINTAINER Michael Braun
EXPOSE 8080/tcp

ENV ROOT_PATH /var/www

RUN apk add bash && \
    mkdir /var/www && \
    mkdir /home/go && \
    addgroup go && adduser -S -G go go && \
    chown -R go:go /var/www && \
    chown -R go:go /home/go

COPY ./lttp /usr/local/bin/lttp

RUN chown -R go:go /usr/local/bin/lttp && \
    chmod +x /usr/local/bin/lttp

USER go:go

CMD ["lttp"]